/************************************************************************************/
/* FILE		      : adc.c   									                                   	    */
/* AUTHOR		    : you																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Offers ADC function ued on PIC18F6680	and PICEBS board     				*/
/*----------------------------------------------------------------------------------*/
/* REVISION     : 1.0 (02-2013) - tested with HI-TECH compiler                      */
/************************************************************************************/
#include <htc.h>
#include <stdint.h>
#include "adc.h"
#include "const.h"

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void ADC_init(void)
{
    //Vref+ = 3,3 V by default, Vref- = 0 V by default
    
    ADCON2bits.ADCS = 1; //Conversion Clock at FOSC/8
    ADCON2bits.ACQT = 2; //set acquisition time 4us
    ADCON2bits.ADFM = 1; //right justified    
    ADCON0bits.ADON = 1; //enable ad
    
}

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
uint16_t ADC_measure(uint8_t channel)
{
    uint8_t i;
    uint32_t retval=0;
    ADCON0bits.CHS = channel;
    
    for(i=0;i<NB_MES;i++)
    {
        ADCON0bits.GODONE = 1; //start conversion
        while(ADCON0bits.GODONE == 1)
        {
            //wait conversion to complete
        }
        retval += (ADRESH<<8)+ADRESL;
    }
    
    return (uint16_t)(retval/NB_MES);
}

uint16_t ADC_get()
{
    uint16_t retval=0;
    retval = (ADRESH<<8)+ADRESL;
    return retval;
}

void ADC_setChannel(uint8_t channel)
{
    ADCON0bits.CHS = channel;
}

void ADC_startMeasures()
{
    //enable interrupt here because we dont want it sooner
    PIR1bits.ADIF = 0; //clear flag
    PIE1bits.ADIE = 1; //enable interrupt
    
    ADC_setChannel(V_CHANNEL); //select first channel
    ADCON0bits.GODONE = 1; //start conversion and catch result in interrupt
}