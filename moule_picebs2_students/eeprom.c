#include <pic18f87k22.h>
#include <pic18.h>

#include "eeprom.h"

    /**
     * Write a byte to eeprom
     * @param addr address to write
     * @param value value to write at specified address
     */
    void ee_write(uint16_t addr, uint8_t value){
        EEADRH=addr>>8;
        EEADR=addr;
        
        EEDATA=value;
        
        EECON1bits.EEPGD=0;
        EECON1bits.CFGS=0;
        EECON1bits.WREN=1;
        
        INTCONbits.GIE=0;
        EECON2=0x55;
        EECON2=0xAA;
       
        EECON1bits.WR=1;
        while(EECON1bits.WR == 1){}
        while(EEIF == 0){}
        EECON1bits.WREN=0; 
        EEIF=0;
        
        INTCONbits.GIE=1;

    }
    
    /**
     * Read a byte from eeprom
     * @param addr address to read
     * @return content of eeprom at specified address
     */
    uint8_t ee_read(uint16_t addr){
        EEADRH=addr>>8;
        EEADR=addr;
        
        EECON1bits.EEPGD=0;
        EECON1bits.CFGS=0;
        EECON1bits.RD=1;
        
        asm("nop");
        
        return EEDATA;        
    }