/************************************************************************************/
/* FILE		      : modbus.h 									                                   	    */
/* AUTHOR		    : you																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Offers MODBUS functions ued on PIC18F6680	and PICEBS board     		*/
/*----------------------------------------------------------------------------------*/
/* REVISION     : 1.0 (02-2013) - tested with HI-TECH compiler                      */
/************************************************************************************/
#include <htc.h>
#include <stdint.h>
#include "uart.h"
#include "crc.h"
#include "const.h"
#ifndef  MODBUS_H
#define MODBUS_H

extern uint16_t inputRegisters[INPUT_REG_SIZE];		// MODBUS table of registers
extern uint16_t holdingRegisters[HOLDING_REG_SIZE];		// MODBUS table of registers

/************************************************************************************/
/* FUNCTION     : MODBUS_init								                                   	    */
/* INPUT		    : -																																	*/
/* OUTPUT		    : - 																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Initialise the MODBUS system                                      */
/*              	Data tables, pointers, ... 																				*/
/************************************************************************************/
void MODBUS_init(void);

/************************************************************************************/
/* FUNCTION     : MODBUS_send    			                                         	    */
/* INPUT				: ... some parameters to be defined ...                      				*/
/* OUTPUT				: -																								  								*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Send the MODBUS packet to serial line			  											*/
/************************************************************************************/
void MODBUS_send( /* TODO */ );

/************************************************************************************/
/* FUNCTION     : MODBUS_receiveAndAnswer                                      	    */
/* INPUT				: - 																							          				*/
/* OUTPUT				: return code could be useful  									  								*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : This function analyses a received packet, check it and						*/
/*                returns an answer if all was OK.																	*/
/************************************************************************************/
void MODBUS_receiveAndAnswer(void);

#endif
