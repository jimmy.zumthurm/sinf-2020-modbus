/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */
#include <stdio.h>
#endif

#include "lcd_highlevel.h"          /* User funct/params, such as InitApp */
#include "lcd_bigfont.h"
#include "tsc.h"
#include "uart.h"
#include "adc.h"
#include "modbus.h"
#include "eeprom.h"
#include "pwm.h"
#include <stdint.h>
#include "const.h"

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

//var
uint16_t iOffset = 0;
uint32_t temp32 = 0;
uint8_t F_modbusReceived = 0;

void initTimer1(){
    T1CONbits.TMR1CS = 1; //select FOSC 
    T1CONbits.T1CKPS = 0; //set prescaler at 1
    TMR1 = TMR1_VAL; //250 hz
    
    TMR1IF = 0;
    TMR1IE = 1;
    PEIE = 1;
    GIE = 1;
    TMR1ON = 0; //wait for first received byte to start timer
}

void initTimer3(){
    T3CONbits.TMR3CS = 1; //select FOSC 
    T3CONbits.T3CKPS = 0; //set prescaler at 1
    TMR3 = TMR3_VAL; //1000hz
    
    TMR3IF = 0;
    TMR3IE = 1;
    PEIE = 1;
    GIE = 1;
    TMR3ON = 0; //wait for start measures
}

//get current offset
void initIOffset()
{
    PWM_set(0); // set PWM at 0 and wait to apply
    __delay_ms(5); //
    iOffset = ADC_measure(I_CHANNEL);
}

void main(void)
{    
    PWM_init();
    initTimer1();
    initTimer3();
    UART_init();
    ADC_init();
    MODBUS_init();
    
    initIOffset();
    ADC_startMeasures();
    
	for(;;)
	{
        if(F_modbusReceived == 1)
        {
            F_modbusReceived = 0;
            MODBUS_receiveAndAnswer();
        }
	}
}


