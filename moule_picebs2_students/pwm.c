/************************************************************************************/
/* FILE		      : pwm.c   									                                   	    */
/* AUTHOR		    : you																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Offers PWM function ued on PIC18F6680	and PICEBS board     				*/
/*----------------------------------------------------------------------------------*/
/* REVISION     : 1.0 (02-2013) - tested with HI-TECH compiler                      */
/************************************************************************************/
#include <htc.h>
#include <stdint.h>
#include "pwm.h"

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void PWM_init(void)
{
	PR2 = 249 ;
    TRISGbits.TRISG3 = 0; //set as output
    T2CONbits.T2CKPS = 1; //timer2 prescaler set as 4
    CCP4CONbits.CCP4M = 15; //set pwm mode
    TMR2ON = 1;
}

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void PWM_set(uint16_t duty)
{
    //we can assume duty on 10 bits
    CCPR4L = duty >> 2;
    CCP4CONbits.DC4B = duty & 3;
}

