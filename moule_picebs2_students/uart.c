/************************************************************************************/
/* FILE		      : uart.c   									                                   	    */
/* AUTHOR		    : you																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Offers UART function ued on PIC18F6680	and PICEBS board   				*/
/*----------------------------------------------------------------------------------*/
/* REVISION     : 1.0 (02-2013) - tested with HI-TECH compiler                      */
/************************************************************************************/
#include <htc.h>
#include <stdint.h>
#include "pwm.h"
#include "uart.h"

uint8_t	rxBuffer[X_SIZE];
uint8_t	txBuffer[X_SIZE]; //40 bytes maybe not long enough

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void UART_init(void)
{
	//Interrupt config
    GIE = 1;
    PEIE = 1;
    
    //UART config
    TRISG = 0;          //force alim on
    PORTGbits.RG0 = 1;  //
    
    SPBRG1 = 12;
    TXSTA1bits.BRGH = 0; //low speed baudrate   
    BAUDCON1bits.BRG16 = 0;
    
    TXSTA1bits.SYNC = 0;  // enable asynchronous serial port   
    RCSTA1bits.SPEN = 1;  //
    
    RCSTA1bits.CREN = 1; // enable receiver
    
    PIE1bits.RC1IE = 1; // receiver interrupt enable
    PIR1bits.RC1IF = 0; // clear flag  
    
    TXSTA1bits.TX9 = 1;  //enable 9th bit
    TXSTA1bits.TX9D = 1; //set stop bit
    
    TXSTA1bits.TXEN = 1; //enable transmission
}

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void UART_write(uint8_t data)
{
    while(TX1IF != 1)
    {
        //wait ready to send
    }
    TXREG1 = data; //write in TXREG1 = send     
}

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
uint8_t UART_read(void)
{
	return RCREG1;
}

