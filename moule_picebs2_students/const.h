
// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef CONST_H
#define	CONST_H

#include <xc.h> // include processor files - each processor file is guarded.  

#define V_REG  0
#define I_REG  1
#define V_CHANNEL 1
#define I_CHANNEL 2
#define NB_MES 64
#define GAIN 67

#define X_SIZE 40

#define INPUT_REG_SIZE 2
#define HOLDING_REG_SIZE 2
#define HR_ZCONTROL 0
#define HR_ADDRESS 1
#define DEFAULT_ADDRESS 0x80

#define TMR1_VAL 33536
#define TMR3_VAL 63536

#endif	/* CONST_H */

