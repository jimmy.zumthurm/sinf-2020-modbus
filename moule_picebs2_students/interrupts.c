/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include "tsc.h"
#include "modbus.h"
#include "uart.h"
#include "const.h"
#include "adc.h"
#endif

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/
/* High-priority service */

extern uint16_t iOffset;
extern uint8_t F_modbusReceived;
uint8_t channel_sel = V_CHANNEL;
uint8_t countMes = 0;
uint32_t temp32 = 0;

void interrupt high_isr(void)
{
	//interrupt when byte received over serial line
    if( (PIR1bits.RC1IF == 1) && (PIE1bits.RC1IE == 1) )
    {
        PIR1bits.RC1IF = 0; //clear flag
        TMR1 = 33536; //RESET TMR1 VALUE
        //if first byte => init
        if(TMR1ON == 0)
        {
            TMR1ON = 1; // start timer
            rxCnt = 0; //clear count
        }
        rxBuffer[rxCnt] = UART_read();
        rxCnt++;
    }
    
    //interrupt when timer1 reaches max value, which means frame is completely received
    //i.e. ready to decode
    if((TMR1IE == 1)&&(TMR1IF ==1))
    {
        TMR1IF = 0; //reset flag
        TMR1ON = 0; //stop timer
        TMR1 = TMR1_VAL; //reset timer
        
        F_modbusReceived = 1;
    }
    
    //timer to force pause between conversions
    if((TMR3IE == 1)&&(TMR3IF ==1))
    {
        TMR3IF = 0; //reset flag
        TMR3ON = 0; //stop timer
        TMR3 = TMR3_VAL; //reset timer
        
        GODONE = 1; //start conversion
    }
    
    if((ADIF == 1) && (ADIE == 1))
    {
        ADIF = 0; //clear flag
        
        switch(channel_sel)
        {
            case V_CHANNEL :
            {
                if(countMes < NB_MES)
                {
                    temp32 += ADC_get();
                    countMes++;
                    TMR3ON = 1;
                }
                else
                {
                    temp32 = temp32/NB_MES;
                    inputRegisters[V_REG] = (uint16_t)((temp32 * 3300)/4095);
                    
                    ADC_setChannel(I_CHANNEL);
                    channel_sel = I_CHANNEL;
                    countMes = 0;
                    temp32 = 0;
                    TMR3ON = 1;
                }
                break;
            }
            case I_CHANNEL :
            {
                if(countMes < NB_MES)
                {
                    temp32 += ADC_get();
                    countMes++;
                    TMR3ON = 1;
                }
                else
                {
                    temp32 = temp32/NB_MES;
                    temp32 = temp32-iOffset;
                    temp32 = (temp32 * 3300)/4095;
                    temp32 = (temp32 * 1000) / GAIN;
                    inputRegisters[I_REG] = (uint16_t)(temp32/3);
                    
                    ADC_setChannel(V_CHANNEL);
                    channel_sel = V_CHANNEL;
                    countMes = 0;
                    temp32 = 0;
                    TMR3ON = 1;
                }
                break;
            }
            default :
            {
                break;
            }
                
        }
    }
}

