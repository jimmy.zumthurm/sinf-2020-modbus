/************************************************************************************/
/* FILE		      : modbus.c 									                                   	    */
/* AUTHOR		    : you																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Offers MODBUS functions ued on PIC18F6680	and PICEBS board     		*/
/*----------------------------------------------------------------------------------*/
/* REVISION     : 1.0 (02-2013) - tested with HI-TECH compiler                      */
/************************************************************************************/
#include <htc.h>
#include <stdint.h>
#include "uart.h"
#include "crc.h"
#include "pwm.h"
#include "const.h"

uint16_t inputRegisters[INPUT_REG_SIZE];		// MODBUS table of registers
uint16_t holdingRegisters[HOLDING_REG_SIZE];		// MODBUS table of registers

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void MODBUS_init(void)
{
    holdingRegisters[HR_ADDRESS] = DEFAULT_ADDRESS;
}

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void MODBUS_send(uint8_t *message, uint16_t size)
{
    uint16_t i;
    uint16_t CRC;
    
    txBuffer[0] = holdingRegisters[HR_ADDRESS];
    //copy
    for(i=0;i<size;i++)
    {
        txBuffer[i+1] = *(message+i);
    }
    CRC = CRC16(txBuffer, size+1);
    
    for(i=0;i<size+1;i++)
    {
        UART_write(txBuffer[i]);
    }
    //send CRC LSB first order
    UART_write((uint8_t)CRC);
    UART_write((uint8_t)(CRC>>8));
    
    
}

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void MODBUS_receiveAndAnswer(void)
{
    uint16_t addr;
    uint16_t qtty;
    uint16_t val;
    uint8_t answer[X_SIZE-1];
    uint16_t i;
    uint16_t CRC;
    uint16_t size;
    
    //check if the message is for this server, if not then ignore it
	if(rxBuffer[0]==holdingRegisters[HR_ADDRESS]) //MODBUS and MODBUS-over-serial-line frame : 1 address byte - 1 byte function code - data - 2 bytes CRC(LSB)
    {
        //check CRC
        CRC = (rxBuffer[rxCnt-1]<<8) + rxBuffer[rxCnt-2]; // reversed OK
        if(CRC != CRC16(rxBuffer, rxCnt-2))
        {
            return; // nothing is sent back => will trigger timeout
        }
        
        //check function code 
        switch(rxBuffer[1])
        {
            
            case 0x03: //read holding register
            {
                addr = ((uint16_t)rxBuffer[2]<<8) + rxBuffer[3];
                qtty = ((uint16_t)rxBuffer[4]<<8) + rxBuffer[5];
                if((addr+qtty)>HOLDING_REG_SIZE)
                {
                    //SEND BACK ERROR
                    answer[0] = 0x83;
                    answer[1] = 2;
                    size = 2;
                    MODBUS_send(answer, size);
                    return;
                }
                //answer
                size = 2+qtty*2;
                answer[0] = 0x03;
                answer[1] = qtty*2;
                for(i=0; i<qtty ;i++)
                {
                    answer[2+2*i] = (uint8_t)(inputRegisters[addr+i]>>8);
                    answer[3+2*i] = (uint8_t)inputRegisters[addr+i];
                }
                
                MODBUS_send(answer, size);
                break;
            }
            case 0x04: //read input register
            {
                addr = (rxBuffer[2]<<8) + rxBuffer[3];
                qtty = (rxBuffer[4]<<8) + rxBuffer[5];
                if((addr+qtty)>INPUT_REG_SIZE)
                {
                    //SEND BACK ERROR
                    answer[0] = 0x84;
                    answer[1] = 2;
                    size = 2;
                    MODBUS_send(answer, size);
                    return;
                }
                //answer
                size = 2+qtty*2;
                answer[0] = 0x04;
                answer[1] = qtty*2;
                for(i=0; i<qtty ;i++)
                {
                    answer[2+2*i] = (uint8_t)(inputRegisters[addr+i]>>8);
                    answer[3+2*i] = (uint8_t)inputRegisters[addr+i];
                }
                
                MODBUS_send(answer, size);
                break;
            }
            
            case 0x06 : //write single holding register
            {
                addr = (rxBuffer[2]<<8) + rxBuffer[3];
                val = (rxBuffer[4]<<8) + rxBuffer[5];
                if(addr>=HOLDING_REG_SIZE)
                {
                    //SEND BACK ERROR
                    answer[0] = 0x86;
                    answer[1] = 2;
                    size = 2;
                    MODBUS_send(answer, size);
                    return;
                }
                //test if value greater than 10 bit
                if(val>0x3FF && addr == HR_ZCONTROL)
                {
                    //SEND BACK ERROR
                    answer[0] = 0x86;
                    answer[1] = 3;
                    size = 2;
                    MODBUS_send(answer, size);
                    return;
                }
                holdingRegisters[addr] = val;
                
                if(addr == HR_ZCONTROL)
                {
                    PWM_set(holdingRegisters[HR_ZCONTROL]);
                }
                
                size = 5;
                for(i=0;i<5;i++)
                {
                    answer[i] = rxBuffer[i+1];
                }
                MODBUS_send(answer, size);
                break;
            }
            
            default :
            {
                //SEND BACK ERROR
                answer[0] = 0x80+rxBuffer[1];
                answer[1] = 1;
                size = 2;
                MODBUS_send(answer, size);
                return;
            }
             
        }
    }
    return;
}

